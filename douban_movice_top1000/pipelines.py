# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import MySQLdb

class DoubanMoviceTop1000Pipeline:
    def process_item(self, item, spider):
        return item

class CSVPipeline(object):
    filen = None
    index = 0
    def open_spider(self,spider):
        self.file = open('douban_movice_top1000.csv','a',encoding='utf-8')
    def process_item(self, item, spider):
        if self.index == 0:
            column_name = 'name,director,performer,type,district,year,score,evaluate,Upload_time\n'
            self.file.write(column_name)
            self.index = 1
        all_data = item['name']+','+item['director']+','+item['performer']+','+item['type']+','+item['district']+','+item['year']+','+item['score']+','+item['evaluate']+','+item['Upload_time']+'\n'
        self.file.write(all_data)
        return item
    def close_spider(self,spider):
        self.file.close()


class MySQLPipeline(object):

    def open_spider(self,spider):
        db_name = spider.settings.get('MYSQL_DB_NAME','douban_movice')
        host = spider.settings.get('MYSQL_HOST','localhost')
        user = spider.settings.get('MYSQL_USER','root')
        password = spider.settings.get('MYSQL_PASSWORD','123456')
        self.db_conn = MySQLdb.connect(db=db_name,
                                       host=host,
                                       user=user,
                                       password=password,
                                       charset='utf8')
        self.db_cursor = self.db_conn.cursor()
    def process_item(self, item,spider):
        values = (item['name'],item['director'],item['performer'],item['type'],item['district'],item['year'],item['score'],item['evaluate'],item['Upload_time'])
        sql = 'insert into movice_top1000(name,director,performer,type,district,year,score,evaluate,upload_time) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        self.db_cursor.execute(sql,values)
        return item
    def close_spider(self,spider):
        self.db_conn.commit()
        self.db_cursor.close()
        self.db_conn.close()