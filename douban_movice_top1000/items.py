# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DoubanMoviceTop1000Item(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field() #电影名称
    director = scrapy.Field() #导演
    performer = scrapy.Field() #演员
    type = scrapy.Field() #类型
    district = scrapy.Field() # 制片地区
    year = scrapy.Field() # 上映年份
    score = scrapy.Field() #评分
    evaluate = scrapy.Field() #评价人数
    Upload_time = scrapy.Field() #上传时间


