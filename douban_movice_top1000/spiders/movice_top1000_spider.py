#coding:utf-8

from scrapy.spiders import Request
from scrapy import Spider
from douban_movice_top1000.items import DoubanMoviceTop1000Item

class MoviceTop1000Spider(Spider):
    name = 'movice'

    def start_requests(self):
        url = 'https://www.douban.com/doulist/109986428/?start=1'
        yield Request(url)

    def parse(self, response):
        item = DoubanMoviceTop1000Item()
        select_list = response.xpath('//div[@class="doulist-item"]')
        for one_list in select_list:
            try:
                name_list = one_list.xpath('div/div/div[@class="title"]/a/text()').extract()
                # print(name_list)
                if len(name_list) == 1:
                    name = name_list[0].replace('\n','').strip()
                    # print(name)
                elif len(name_list) == 2:
                    name = name_list[1].replace('\n','').strip()
                    # print(name)
                abstract_list = one_list.xpath('div/div/div[@class="abstract"]/text()').extract()
                director = abstract_list[0].strip().replace('导演: ','')
                # print(director)
                performer = abstract_list[1].strip().replace('主演: ','')
                # print(performer)
                type = abstract_list[2].strip().replace('类型: ','')
                # print(type)
                district = abstract_list[3].strip().replace('制片国家/地区: ','')
                # print(district)
                year = abstract_list[4].strip().replace('年份: ','')
                # print(year)
                score = one_list.xpath('div/div/div[@class="rating"]/span[2]/text()').extract()[0]
                # print(score)
                evaluate = one_list.xpath('div/div/div[@class="rating"]/span[3]/text()').extract()[0].replace('(','').replace('人评价)','')
                # print(evaluate)
                Upload_time = one_list.xpath('div/div[@class="ft"]/div[1]/time/text()').extract()[0]
                # print(Upload_time)
            except:
                pass
            item['name'] = name
            item['director'] = director
            item['performer'] = performer
            item['type'] = type
            item['district'] = district
            item['year'] = year
            item['score'] = score
            item['evaluate'] = evaluate
            item['Upload_time'] = Upload_time
            yield item

        next_url =  response.xpath('//div[@class="paginator"]/span[@class="next"]/a/@href').extract()[0]
        if next_url:
            yield Request(next_url)



